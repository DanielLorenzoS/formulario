SET NAMES 'UTF8MB4';
DROP DATABASE IF EXISTS examen;
CREATE DATABASE IF NOT EXISTS examen DEFAULT CHARACTER SET UTF8MB4;
USE examen;
CREATE TABLE hoteles(
id_htl                        INTEGER NOT NULL AUTO_INCREMENT,
nombre                        VARCHAR(40) NOT NULL,
direccion                     VARCHAR(240) NOT NULL,
telefono                      VARCHAR(20) NOT NULL,
correo		                  VARCHAR(240) NOT NULL,
id_grt                        INTEGER ,
PRIMARY KEY(id_htl)
)DEFAULT CHARACTER SET UTF8MB4;

CREATE TABLE hotel_imagenes(
id_htl_img                       INTEGER NOT NULL AUTO_INCREMENT,
nombre                        	VARCHAR(40) NOT NULL,
id_htl                     		INTEGER ,
PRIMARY KEY(id_htl_img)
)DEFAULT CHARACTER SET UTF8MB4;

ALTER TABLE `hotel_imagenes` ADD CONSTRAINT `htl_img_fk0` FOREIGN KEY (`id_htl`) REFERENCES `hoteles`(`id_htl`);

CREATE TABLE gerentes(
id_grt                        INTEGER NOT NULL AUTO_INCREMENT,
nombre                        VARCHAR(40) NOT NULL,
apellido_paterno              VARCHAR(240) NOT NULL,
apellido_materno              VARCHAR(240) NOT NULL,
telefono		              VARCHAR(240) NOT NULL,
PRIMARY KEY(id_grt)
)DEFAULT CHARACTER SET UTF8MB4;

ALTER TABLE `hoteles` ADD CONSTRAINT `hotel_fk0` FOREIGN KEY (`id_grt`) REFERENCES `gerentes`(`id_grt`);

CREATE TABLE gerente_imagenes(
id_grt_img                       INTEGER NOT NULL AUTO_INCREMENT,
nombre                        	VARCHAR(40) NOT NULL,
id_grt                     		INTEGER ,
PRIMARY KEY(id_grt_img)
)DEFAULT CHARACTER SET UTF8MB4;

ALTER TABLE `gerente_imagenes` ADD CONSTRAINT `grt_img_fk0` FOREIGN KEY (`id_grt`) REFERENCES `gerentes`(`id_grt`);

CREATE TABLE habitaciones(
id_hbt                        INTEGER NOT NULL AUTO_INCREMENT,
tipo						VARCHAR(20) NOT NULL,
id_htl                     INTEGER ,
PRIMARY KEY(id_hbt)
)DEFAULT CHARACTER SET UTF8MB4;

ALTER TABLE `habitaciones` ADD CONSTRAINT `hbt_fk0` FOREIGN KEY (`id_htl`) REFERENCES `hoteles`(`id_htl`);

CREATE TABLE habitacion_imagenes(
id_hbt_img                       INTEGER NOT NULL AUTO_INCREMENT,
nombre                        	VARCHAR(40) NOT NULL,
id_hbt                     		INTEGER ,
PRIMARY KEY(id_hbt_img)
)DEFAULT CHARACTER SET UTF8MB4;

ALTER TABLE `habitacion_imagenes` ADD CONSTRAINT `hbt_img_fk0` FOREIGN KEY (`id_hbt`) REFERENCES `habitaciones`(`id_hbt`);


INSERT INTO gerentes(id_grt,nombre,apellido_paterno,apellido_materno,telefono) VALUES('1','Bruno','Hernandez','Hernandez','5575926583');
INSERT INTO gerentes(id_grt,nombre,apellido_paterno,apellido_materno,telefono) VALUES('2','Daniel','Lorenzo','Gonzalez','55920671208');
INSERT INTO gerentes(id_grt,nombre,apellido_paterno,apellido_materno,telefono) VALUES('3','Mariano','Hernandez','Solares','5528672960');
INSERT INTO gerentes(id_grt,nombre,apellido_paterno,apellido_materno,telefono) VALUES('4','Berenice','Hernandez','Solares','5528672960');
INSERT INTO gerentes(id_grt,nombre,apellido_paterno,apellido_materno,telefono) VALUES('5','Aldo','Barrios','Solares','5528672960');
INSERT INTO gerentes(id_grt,nombre,apellido_paterno,apellido_materno,telefono) VALUES('6','Mario','Rubin','Solares','5528672960');

INSERT INTO hoteles(id_htl,nombre,direccion,telefono,correo,id_grt) VALUES('1','Princesa','Tlalpan 53','5534758265','princess@hmal.com', 3);
INSERT INTO hoteles(id_htl,nombre,direccion,telefono,correo,id_grt) VALUES('2','Sfera','Av.Rio','5530091636','sfera@hmal.com', 4);
INSERT INTO hoteles(id_htl,nombre,direccion,telefono,correo,id_grt) VALUES('3','Xola','San Pedro Martir','5564927592','xooo@hmal.com', 2);
INSERT INTO hoteles(id_htl,nombre,direccion,telefono,correo,id_grt) VALUES('4','XolaLola','San Pedro Martir','5564927592','xooo@hmal.com', 1);

INSERT INTO habitaciones(id_hbt,tipo,id_htl) VALUES('1','Suite',1);
INSERT INTO habitaciones(id_hbt,tipo,id_htl) VALUES('2','Master Suite',2);
INSERT INTO habitaciones(id_hbt,tipo,id_htl) VALUES('3','Penthouse',null);
INSERT INTO habitaciones(id_hbt,tipo,id_htl) VALUES('4','Individual',null);
INSERT INTO habitaciones(id_hbt,tipo,id_htl) VALUES('5','Doble',4);

INSERT INTO hotel_imagenes(id_htl_img,nombre,id_htl) VALUES('1','hotel1.jpg', 1);
INSERT INTO hotel_imagenes(id_htl_img,nombre,id_htl) VALUES('2','hotel2.jpg', 2);
INSERT INTO hotel_imagenes(id_htl_img,nombre,id_htl) VALUES('3','hotel3.jpeg', 3);
INSERT INTO hotel_imagenes(id_htl_img,nombre,id_htl) VALUES('4','hotel4.jpg', 4);

INSERT INTO gerente_imagenes(id_grt_img,nombre,id_grt) VALUES('1','grt1.jpeg',1);
INSERT INTO gerente_imagenes(id_grt_img,nombre,id_grt) VALUES('2','grt2.jpeg',2);
INSERT INTO gerente_imagenes(id_grt_img,nombre,id_grt) VALUES('3','grt3.jpeg',3);
INSERT INTO gerente_imagenes(id_grt_img,nombre,id_grt) VALUES('4','grt4.jpeg',4);
INSERT INTO gerente_imagenes(id_grt_img,nombre,id_grt) VALUES('5','grt5.jpeg',5);
INSERT INTO gerente_imagenes(id_grt_img,nombre,id_grt) VALUES('6','grt6.jpeg',6);

INSERT INTO habitacion_imagenes(id_hbt_img,nombre,id_hbt) VALUES('1','hbt1.jpeg', 1);
INSERT INTO habitacion_imagenes(id_hbt_img,nombre,id_hbt) VALUES('2','hbt2.jpeg', 2);
INSERT INTO habitacion_imagenes(id_hbt_img,nombre,id_hbt) VALUES('3','hbt3.jpeg', 3);
INSERT INTO habitacion_imagenes(id_hbt_img,nombre,id_hbt) VALUES('4','hbt4.jpeg', 4);
INSERT INTO habitacion_imagenes(id_hbt_img,nombre,id_hbt) VALUES('5','hbt5.jpeg', 5);

select * from hoteles;
select * from gerentes;
select * from habitaciones;
select * FROM hotel_imagenes;
select * FROM gerente_imagenes;
select * FROM habitacion_imagenes;
