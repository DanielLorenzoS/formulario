import express from "express";
import routes from "./routes/index.js";
import db from "./config/db.js";
import fileUpload from "express-fileupload";
import path from "path";
import session from "express-session";
import { nanoid } from "nanoid";

import filesPayloadExists from './middleware/filesPayloadExists.js';
import fileExtLimiter from './middleware/fileExtLimiter.js';
import fileSizeLimiter from './middleware/fileSizeLimiter.js';
import { Gerente } from "./models/Gerente.js";
import { Habitacion } from "./models/Habitacion.js";
import { Hotel_Imagen } from "./models/Hotel_Imagen.js";
import { Hotel } from "./models/Hotel.js";
import { Gerente_Imagen } from "./models/Gerente_Imagen.js";
import { Habitacion_Imagen } from "./models/Habitacion_Imagen.js";

global.id_global = 1;

const app = express();

const port = process.env.PORT || 1800;

app.listen(port, () => {
  console.log(`Servidor iniciando en el puerto` + port);
});

app.set("view engine", "pug");

app.use(express.urlencoded({ extended: true }));

app.use(express.json());

app.use(express.static("public"));

app.use(session({
  secret: nanoid(),
  resave: true,
  saveUninitialized: true
}));

app.use(async (req, res, next) => {
  const ano = new Date();
  res.locals.tiempo = " " + ano.getFullYear();
  try {
    var arrayDeCadenas = req.url.split("/")
    if (req.url === "/publicidad") {
      const hoteles = await Hotel.findAll({
        include: [{ model: Gerente }, { model: Habitacion }, { model: Hotel_Imagen }],
      });
      res.render("publicidad", {
        hoteles
      });
    }
    else if(req.url.includes(`/individual/${arrayDeCadenas[arrayDeCadenas.length - 1]}`)){
      let hotel = await Hotel.findOne({
        include: [{ model: Gerente }, { model: Habitacion }, { model: Hotel_Imagen }],
        where: {
          id_htl: arrayDeCadenas[arrayDeCadenas.length - 1]
        }
      });

      /* let img_gerente = await Gerente_Imagen.findOne({
        where: {
          id_grt: hotel.dataValues.gerente.id_grt
        }
      })
      let habitacion = await Habitacion.findAll({
        where: {
          id_hbt: hotel.habitaciones.map(e => e.id_hbt)
        },
        include: { model: Habitacion_Imagen }
      })
      let images = habitacion.map(e => e.habitacion_imagenes[0].nombre)

      res.render("individual", {
        hotel,
        imagen_gerente: img_gerente.nombre,
        images
      }); */
      res.send(hotel)
    }
    else if (req.url === "/home") {
      const {
        usuario,
        clave
      } = req.body;
      if (usuario === "demo" && clave === "123") {
        req.session.nombre = "FES";
        req.session.rol = "adm";
        res.render("home", {
          pagina: "datos",
          usuario: req.session.nombre
        })
      } else {
        res.render("login", {
          pagina: "Credenciales"
        });
      }
    } else {
      if (req.session.rol === undefined) {
        res.render("login", {
          pagina: "Credenciales"
        });
      } else {
        return next();
      }

    }

  } catch (e) {
    res.render("login", {
      pagina: "Credenciales"
    });
  }

});

app.use("/", routes);

db.authenticate()
  .then(() => console.log("Conected successfull"))
  .catch((error) => console.log(error));

app.get("/", (req, res) => {
  res.sendFile(path.join("hotel.pug"))
});

app.post('/upload',
  fileUpload({ createParentPath: true }),
  filesPayloadExists,
  fileExtLimiter(['.png', '.jpg', '.jpeg']),
  fileSizeLimiter,
  (req, res) => {
    const files = req.files
    console.log(files)

    Object.keys(files).forEach(key => {
      const filepath = path.join('public/img/hotel', files[key].name)
      files[key].mv(filepath, (err) => {
        if (err) return res.status(500).json({ status: "error", message: err })
      })
    })

    return res.json({ status: 'success', message: Object.keys(files).toString() })
  }
)
app.post('/upload_grt',
  fileUpload({ createParentPath: true }),
  filesPayloadExists,
  fileExtLimiter(['.png', '.jpg', '.jpeg']),
  fileSizeLimiter,
  (req, res) => {
    const files = req.files
    console.log(files)

    Object.keys(files).forEach(key => {
      const filepath = path.join('public/img/gerente', files[key].name)
      files[key].mv(filepath, (err) => {
        if (err) return res.status(500).json({ status: "error", message: err })
      })
    })

    return res.json({ status: 'success', message: Object.keys(files).toString() })
  }
)
app.post('/upload_hbt',
  fileUpload({ createParentPath: true }),
  filesPayloadExists,
  fileExtLimiter(['.png', '.jpg', '.jpeg']),
  fileSizeLimiter,
  (req, res) => {
    const files = req.files
    console.log(files)

    Object.keys(files).forEach(key => {
      const filepath = path.join('public/img/habitacion', files[key].name)
      files[key].mv(filepath, (err) => {
        if (err) return res.status(500).json({ status: "error", message: err })
      })
    })

    return res.json({ status: 'success', message: Object.keys(files).toString() })
  }
)
