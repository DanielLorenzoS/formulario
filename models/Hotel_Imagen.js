import Sequelize from "sequelize";
import db from '../config/db.js'

export const Hotel_Imagen = db.define("hotel_imagenes", {
    id_htl_img: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true 
    },
    nombre: {
        type: Sequelize.STRING
    },
    id_htl: {
        type: Sequelize.INTEGER
    }
}, { timestamps: false });