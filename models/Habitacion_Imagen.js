import Sequelize from "sequelize";
import db from '../config/db.js'

export const Habitacion_Imagen = db.define("habitacion_imagenes", {
    id_hbt_img: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true 
    },
    nombre: {
        type: Sequelize.STRING
    },
    id_hbt: {
        type: Sequelize.INTEGER
    }
}, { timestamps: false });