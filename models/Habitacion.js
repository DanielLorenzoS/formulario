import Sequelize from "sequelize";
import db from '../config/db.js';
import { Habitacion_Imagen } from './Habitacion_Imagen.js'

export const Habitacion = db.define("habitaciones", {
    id_hbt: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tipo: {
        type: Sequelize.STRING
    },
    id_htl: {
        type: Sequelize.INTEGER
    }
}, { timestamps: false });

Habitacion.hasMany(Habitacion_Imagen, {
    foreignKey: {
        name: "id_hbt"
    }
})

Habitacion_Imagen.belongsTo(Habitacion, {
    foreignKey: {
        name: "id_hbt"
    }
})