import Sequelize from "sequelize";
import db from '../config/db.js'

export const Gerente_Imagen = db.define("gerente_imagenes", {
    id_grt_img: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true 
    },
    nombre: {
        type: Sequelize.STRING
    },
    id_grt: {
        type: Sequelize.INTEGER
    }
}, { timestamps: false });