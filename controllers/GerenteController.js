import { Gerente } from "../models/Gerente.js";
import { Gerente_Imagen } from "../models/Gerente_Imagen.js";
import { Hotel } from "../models/Hotel.js";

const getGerentes = async (req, res) => {

    await Gerente_Imagen.destroy({
        where: {
            id_grt: null
        }
    })

    const gerentes = await Gerente.findAll({
        include: [{ model: Hotel }, { model: Gerente_Imagen }],
    });

    res.render("gerente", {
        gerentes: gerentes
    });
}
export const getAddGerentes = async (req, res) => {
    const gerentes = await Gerente.findAll({
        include: [{ model: Hotel }]
    });

    res.render("addGerente", {
        gerentes: gerentes
    });
}

const setGerente = async (req, res) => {
    let { nombre, apellido_paterno, apellido_materno, telefono, imagen } = req.body;

    let validImage = true;

    let arr = imagen.split('.');
    if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
        validImage = false;
    }

    if (imagen == "") {
        validImage = true;
    }

    const errores = [];
    if (nombre.trim() === "") {
        errores.push({ mensaje: "El nombre no debe estar vacío" });
    }
    if (apellido_paterno.trim() === "") {
        errores.push({ mensaje: "El apellido paterno no debe estar vacío" });
    }
    if (apellido_materno.trim() === "") {
        errores.push({ mensaje: "El apellido materno no debe estar vacío" });
    }
    if (telefono.trim() === "") {
        errores.push({ mensaje: "El telefono no debe estar vacío" });
    }
    if (typeof (imagen) === "object") {
        errores.push({ mensaje: "Sólo puedes escoger una imagen" });
    }
    if (validImage === false) {
        errores.push({ mensaje: "Sólo se permiten los formatos .png .jpg y .jpeg" });
    }
    if (imagen === "") {
        errores.push({ mensaje: "Debes seleccionar una imagen" });
    }
    if (errores.length > 0) {
        const gerentes = await Gerente.findAll({
            include: [{ model: Hotel }]
        });

        res.render("addGerente", {
            gerentes: gerentes,
            errores,
            nombre, apellido_paterno, apellido_materno, telefono
        });
    } else {

        let bln = true;
        let gerentes = await Gerente.findAll()

        gerentes.map(e => {
            if (nombre === e.nombre &&
                apellido_paterno === e.apellido_paterno &&
                apellido_materno === e.apellido_materno &&
                telefono === e.telefono) {
                bln = false;
            }
        })

        if (bln === true) {
            await Gerente.create({
                nombre,
                apellido_materno,
                apellido_paterno,
                telefono
            });

            let newGerente = await Gerente.findOne({
                where: {
                    nombre: nombre
                }
            })

            await Gerente_Imagen.create({
                nombre: imagen,
                id_grt: newGerente.dataValues.id_grt
            })
        }


        res.redirect("/gerente");
    }
}

export const getEditGerente = async (req, res) => {
    const { id_grt } = req.params;

    let gerente = await Gerente.findOne({
        where: {
            id_grt,
        },
    });

    let imagenes = await Gerente_Imagen.findOne({
        where: {
            id_grt
        }
    })

    res.render("editGerente", {
        gerente: gerente,
        imagen: imagenes
    });
};

const editGerente = async (req, res) => {
    const { id_grt } = req.params;
    let { nombre, apellido_paterno, apellido_materno, telefono, imagenes, imagen } = req.body;
    let validImage = true;

    let arr = imagen.split('.');
    if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
        validImage = false;
    }

    if (imagen == "") {
        validImage = true;
    }

    const errores = [];
    if (nombre.trim() === "") {
        errores.push({ mensaje: "El nombre no debe estar vacío" });
    }
    if (apellido_paterno.trim() === "") {
        errores.push({ mensaje: "El apellido paterno no debe estar vacío" });
    }
    if (apellido_materno.trim() === "") {
        errores.push({ mensaje: "El apellido materno no debe estar vacío" });
    }
    if (telefono.trim() === "") {
        errores.push({ mensaje: "El telefono no debe estar vacío" });
    }
    if (validImage === false) {
        errores.push({ mensaje: "Sólo se permiten los formatos .png .jpg y .jpeg" });
    }
    if (imagen === "" && imagenes === undefined) {
        errores.push({ mensaje: "Selecciona al menos una imagen" });
      }
    if (errores.length > 0) {
        const { id_grt } = req.params;

        let gerente = await Gerente.findOne({
            where: {
                id_grt,
            },
        });

        let imagenes = await Gerente_Imagen.findOne({
            where: {
                id_grt
            }
        })

        res.render("editGerente", {
            gerente: gerente,
            imagen: imagenes,
            errores
        });
    } else {

        await Gerente.update(
            {
                nombre,
                apellido_paterno,
                apellido_materno,
                telefono
            },
            {
                where: {
                    id_grt,
                },
            }
        );
        if (imagenes != null && imagen != null) {

            await Gerente_Imagen.update({ id_grt: null }, {
                where: {
                    nombre: imagenes
                }
            })
            await Gerente_Imagen.create({
                nombre: imagen,
                id_grt: id_grt
            })
        }
        if (imagenes != null && imagen == "") {
            await Gerente_Imagen.update({ nombre: imagenes }, {
                where: {
                    id_grt
                }
            })
        }
        if (imagenes == undefined && imagen == "") {
            await Gerente_Imagen.update({ id_grt: null }, {
                where: {
                    id_grt
                }
            })
        }
        if (imagenes == undefined && imagen != "") {

            await Gerente_Imagen.update({ id_grt: null }, {
                where: {
                    id_grt
                }
            })

            await Gerente_Imagen.create({
                nombre: imagen,
                id_grt: id_grt
            })
        }
        res.redirect("/gerente");
    }
};

const removeGerente = async (req, res) => {
    const { id_grt } = req.params;

    await Gerente_Imagen.update({ id_grt: null }, {
        where: {
            id_grt
        }
    })

    await Hotel.update({ id_grt: null }, {
        where: {
            id_grt
        }
    })

    await Gerente.destroy({
        where: {
            id_grt
        }
    })

    res.redirect("/gerente")

};

export { getGerentes, setGerente, removeGerente, editGerente }