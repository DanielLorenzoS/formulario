
export const home = (req, res) => {
  res.render("home", {
    page: "Home",
  });
}

export const login = (req, res) => {
  res.render("login", {
    page: "Login",
  });
}
export const cerrarSesion = (req, res) => {
  req.session.destroy()
  res.render("login");
}


