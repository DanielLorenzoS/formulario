import { Op } from "sequelize";
import { Habitacion } from "../models/Habitacion.js";
import { Habitacion_Imagen } from "../models/Habitacion_Imagen.js";
import { Hotel } from "../models/Hotel.js";

const getHabitaciones = async (req, res) => {

    await Habitacion_Imagen.destroy({
        where: {
            id_hbt: null
        }
    })

    let habitaciones = await Habitacion.findAll({
        include: [{ model: Hotel }, { model: Habitacion_Imagen }]
    });

    res.render("habitacion", {
        habitaciones: habitaciones
    });
    /* habitaciones.map(e => {
        if (e.dataValues.habitacion_imagenes.length > 0) {
            e.dataValues.habitacion_imagenes.map(res => {
                console.log(res.dataValues.nombre);
            })
        }
    }) */
    /* res.send(habitaciones) */
}

export const getAddHabitacion = async (req, res) => {
    res.render("addHabitacion");
}

const setHabitacion = async (req, res) => {
    const { tipo, imagen } = req.body;

    let arrayImagenes = imagen;
    let validImage = true;

    if (typeof (arrayImagenes) == "string") {
        let arr = arrayImagenes.split('.');
        if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
            validImage = false;
        }
    }
    if (typeof (arrayImagenes) == "object") {
        for (let i = 0; i < arrayImagenes.length; i++) {
            let arr = arrayImagenes[i].split('.');
            if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
                validImage = false;
            }
        }
    }
    if (imagen == "") {
        validImage = true;
    }

    const errores = [];
    if (tipo.trim() === "") {
        errores.push({ mensaje: "El tipo de hotel no debe estar vacío" });
    }
    if (validImage === false) {
        errores.push({ mensaje: "Sólo se permiten los formatos .png .jpg y .jpeg" });
    }
    if (imagen === "") {
        errores.push({ mensaje: "Selecciona al menos una imagen" });
    }
    if (errores.length > 0) {

        res.render("addHabitacion", {
            errores, tipo
        });
    } else {

        let newHab = await Habitacion.create({
            tipo
        });

        if (typeof (imagen) == "string") {
            await Habitacion_Imagen.create({
                nombre: imagen,
                id_hbt: newHab.dataValues.id_hbt
            })
        }
        if (typeof (imagen) == "object") {
            for (let i = 0; i < imagen.length; i++) {
                await Habitacion_Imagen.create({
                    nombre: imagen[i],
                    id_hbt: newHab.dataValues.id_hbt
                })
            }
        }

        res.redirect("/habitacion");
    }
};

export const getEditHabitacion = async (req, res) => {
    const { id_hbt } = req.params;

    let habitacion = await Habitacion.findOne({
        where: {
            id_hbt: id_hbt
        },
        include: { model: Habitacion_Imagen }
    })
    let imagenes = habitacion.habitacion_imagenes;
    res.render("editHabitacion", {
        habitacion: habitacion,
        imagenes
    });
};

const editHabitacion = async (req, res) => {
    const { id_hbt } = req.params;
    const { tipo, imagen, imagenes } = req.body;
    let arrayImagenes = imagen;
    let validImage = true;

    if (typeof (arrayImagenes) == "string") {
        let arr = arrayImagenes.split('.');
        if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
            validImage = false;
        }
    }
    if (typeof (arrayImagenes) == "object") {
        for (let i = 0; i < arrayImagenes.length; i++) {
            let arr = arrayImagenes[i].split('.');
            if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
                validImage = false;
            }
        }
    }
    if (imagen == "") {
        validImage = true;
    }
    const errores = [];
    if (tipo.trim() === "") {
        errores.push({ mensaje: "El tipo de habitación no debe estar vacío" });
    }
    if (validImage === false) {
        errores.push({ mensaje: "Sólo se permiten los formatos .png .jpg y .jpeg" });
    }
    if (imagen === "" && imagenes === undefined) {
        errores.push({ mensaje: "Selecciona al menos una imagen" });
    }
    if (errores.length > 0) {
        let habitacion = await Habitacion.findOne({
            where: {
                id_hbt: id_hbt
            },
            include: { model: Habitacion_Imagen }
        })
        let imagenes = habitacion.habitacion_imagenes;
        res.render("editHabitacion", {
            habitacion: habitacion,
            imagenes, errores
        });
    } else {
        await Habitacion.update({ tipo }, {
            where: {
                id_hbt,
            },
        }
        );

        let habitacion = await Habitacion.findOne({
            where: {
                id_hbt
            }
        })

        let promesa = true;

        let imgs = await Habitacion_Imagen.findAll({
            where: {
                id_hbt
            }
        })

        if (typeof (imagen) == "string") {
            imgs.map(e => {
                if (imagen == e.dataValues.nombre) {
                    console.log('se repiten');
                    promesa = false
                }
            })
        }
        if (typeof (imagen) == "object") {
            for (let i = 0; i < imagen.length; i++) {
                imgs.map(e => {
                    if (e.dataValues.nombre == imagen[i]) {
                        console.log('se repiteeeee');
                        promesa = false;
                    }
                })
            }
        }

        if (promesa) {
            if (typeof (imagenes) === "undefined") {
                await Habitacion_Imagen.update({ id_hbt: null }, {
                    where: {
                        id_hbt
                    }
                })
            }
            else {

                let getImage = await Habitacion_Imagen.findAll({
                    where: {
                        nombre: imagenes
                    }
                })
                getImage = getImage.map(e => e.dataValues.nombre)

                if (getImage != imagenes) {

                }
                let image = await Habitacion_Imagen.update({ id_hbt: null }, {
                    where: {
                        nombre: {
                            [Op.not]: imagenes
                        },
                        id_hbt: id_hbt
                    }
                })


            }
            if (typeof (imagen) == "string") {
                await Habitacion_Imagen.create({
                    nombre: imagen,
                    id_hbt: habitacion.dataValues.id_hbt
                })
            }
            if (typeof (imagen) == "object") {
                for (let i = 0; i < imagen.length; i++) {
                    await Habitacion_Imagen.create({
                        nombre: imagen[i],
                        id_hbt: habitacion.dataValues.id_hbt
                    })
                }
            }

            await Habitacion_Imagen.destroy({
                where: {
                    nombre: ''
                }
            });
        }
        res.redirect("/habitacion");
    }
};

export const removeHabitacion = async (req, res) => {
    const { id_hbt } = req.params;

    await Habitacion_Imagen.destroy({
        where: {
            id_hbt: id_hbt
        },
    });

    await Habitacion.destroy({
        where: {
            id_hbt: id_hbt
        },
    });

    res.redirect("/habitacion");
};

export { getHabitaciones, setHabitacion, editHabitacion }