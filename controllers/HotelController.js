import { Op } from "sequelize";
import { Gerente } from "../models/Gerente.js";
import { Gerente_Imagen } from "../models/Gerente_Imagen.js";
import { Habitacion } from "../models/Habitacion.js";
import { Habitacion_Imagen } from "../models/Habitacion_Imagen.js";
import { Hotel } from "../models/Hotel.js";
import { Hotel_Imagen } from "../models/Hotel_Imagen.js";


const getHoteles = async (req, res) => {

  await Hotel_Imagen.destroy({
    where: {
      id_htl: null
    }
  })

  await Hotel_Imagen.destroy({
    where: {
      nombre: ''
    }
  })

  let arrayGerentes = [];

  const gerentes = await Gerente.findAll();

  const habitaciones = await Habitacion.findAll({
    where: {
      id_htl: null
    }
  });

  const hoteles = await Hotel.findAll({
    include: [{ model: Gerente }, { model: Habitacion }, { model: Hotel_Imagen }]
  });

  gerentes.map(e => {
    arrayGerentes.push(e.dataValues);
  })

  hoteles.map(e => {
    if (e.id_grt != null) {
      let temporal = arrayGerentes.filter(element => element.id_grt != e.id_grt)
      arrayGerentes = temporal;
    }
  })

  const imagenes = await Hotel_Imagen.findAll();

  res.render("hotel", {
    hoteles: hoteles,
    gerentes: arrayGerentes,
    habitaciones: habitaciones,
    imagenes
  });
};

export const getAddHoteles = async (req, res) => {
  let arrayHoteles = [];

  let arrayGerentes = await Gerente.findAll();


  let habitaciones = await Habitacion.findAll({
    where: {
      id_htl: null
    }
  });

  const hoteles = await Hotel.findAll({
    include: [{ model: Gerente }, { model: Habitacion }]
  });

  hoteles.map(e => {
    if (e.id_grt != null) {
      let temporal = arrayGerentes.filter(element => element.id_grt != e.id_grt)
      arrayGerentes = temporal;
    }
  });

  arrayGerentes = arrayGerentes.map(e => e.nombre);

  res.render("addHotel", {
    hoteles: hoteles,
    gerentes: arrayGerentes,
    habitaciones: habitaciones
  });
};

const setHoteles = async (req, res) => {
  const { nombre, direccion, telefono, correo, imagen, gerente, habitaciones } = req.body;
  const errores = [];

  let arrayImagenes = imagen;
  let validImage = true;

  if (typeof (arrayImagenes) == "string") {
    let arr = arrayImagenes.split('.');
    if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
      validImage = false;
    }
  }
  if (typeof (arrayImagenes) == "object") {
    for (let i = 0; i < arrayImagenes.length; i++) {
      let arr = arrayImagenes[i].split('.');
      if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
        validImage = false;
      }
    }
  }
  if (imagen == "") {
    validImage = true;
  }

  if (nombre.trim() === "") {
    errores.push({ mensaje: "El nombre no debe estar vacío" });
  }
  if (direccion.trim() === "") {
    errores.push({ mensaje: "La direccion no debe estar vacía" });
  }
  if (telefono.trim() === "") {
    errores.push({ mensaje: "El telefono no debe estar vacío" });
  }
  if (correo.trim() === "") {
    errores.push({ mensaje: "El correo no debe estar vacío" });
  }
  if (gerente === undefined) {
    errores.push({ mensaje: "Seleccione un gerente" });
  }
  if (habitaciones === undefined) {
    errores.push({ mensaje: "Seleccione minimo una habitación" });
  }
  if (validImage === false) {
    errores.push({ mensaje: "Sólo se permiten los formatos .png .jpg y .jpeg" });
  }
  if (imagen === "") {
    errores.push({ mensaje: "Selecciona al menos una imagen" });
  }
  if (errores.length > 0) {

    let arrayHoteles = [];

    let arrayGerentes = await Gerente.findAll();


    let habitaciones = await Habitacion.findAll({
      where: {
        id_htl: null
      }
    });

    const hoteles = await Hotel.findAll({
      include: [{ model: Gerente }, { model: Habitacion }]
    });

    hoteles.map(e => {
      if (e.id_grt != null) {
        let temporal = arrayGerentes.filter(element => element.id_grt != e.id_grt)
        arrayGerentes = temporal;
      }
    });

    arrayGerentes = arrayGerentes.map(e => e.nombre);

    hoteles.map(e => {
      if (e.id_grt != null) {
        let temporal = arrayGerentes.filter(element => element.id_grt != e.id_grt)
        arrayGerentes = temporal;
      }
    });


    res.render("addHotel", {
      hoteles: hoteles,
      gerentes: arrayGerentes,
      habitaciones: habitaciones,
      errores,
      nombre, direccion, telefono, correo, gerente, habitaciones
    });
  } else {


    let getGerente = await Gerente.findOne({
      where: {
        nombre: gerente
      }
    });

    let hotel = await Hotel.create({
      nombre,
      direccion,
      telefono,
      correo,
      id_grt: getGerente.id_grt
    });

    await Habitacion.update({ id_htl: hotel.dataValues.id_htl }, {
      where: {
        tipo: habitaciones
      }
    });

    if (typeof (imagen) == "string") {
      await Hotel_Imagen.create({
        nombre: imagen,
        id_htl: hotel.dataValues.id_htl
      })
    }
    if (typeof (imagen) == "object") {
      for (let i = 0; i < imagen.length; i++) {
        await Hotel_Imagen.create({
          nombre: imagen[i],
          id_htl: hotel.dataValues.id_htl
        })
      }
    }
    res.redirect("/hotel");
  }
};

export const getEditHoteles = async (req, res) => {
  const { id_htl } = req.params;
  let arrayGerentes = [];
  let arrayHoteles = [];

  const hotel = await Hotel.findOne({
    where: {
      id_htl
    }
  });

  let gerentes = await Gerente.findAll();
  arrayGerentes = gerentes.map(e => e.dataValues);
  let hoteles = await Hotel.findAll();
  arrayHoteles = hoteles.map(e => e.dataValues);

  arrayHoteles.map(e => {
    if (e.id_grt != null) {
      let temporal = arrayGerentes.filter(element => element.id_grt != e.id_grt)
      arrayGerentes = temporal;
    }
  })

  if (hotel.dataValues.id_grt != null) {
    const grt = await Gerente.findOne({
      where: {
        id_grt: hotel.dataValues.id_grt
      }
    });

    hotel.dataValues.id_grt = grt.dataValues.nombre;

    arrayGerentes.unshift(grt);
  }

  const hbt = await Habitacion.findAll({
    where: {
      id_htl: id_htl
    }
  });

  let habitaciones = await Habitacion.findAll({
    where: {
      id_htl: null
    }
  });
  if (hbt.length === 1) {
    habitaciones.unshift(hbt[0]);
  } else {
    for (let i = 0; i < hbt.length; i++) {
      habitaciones.unshift(hbt[i]);
    }
  }

  let imagenes = await Hotel_Imagen.findAll({
    where: {
      id_htl
    }
  });

  res.render("editHotel", {
    hotel: hotel,
    gerentes: arrayGerentes,
    habitaciones: habitaciones,
    imagenes
  })
};

const editHoteles = async (req, res) => {
  const { id_htl } = req.params;
  let { nombre, direccion, telefono, correo, id_grt, habitaciones, imagenes, imagen } = req.body;
  let toNull;
  let arrayImagenes = imagen;
  let validImage = true;

  if (typeof (arrayImagenes) == "string") {
    let arr = arrayImagenes.split('.');
    if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
      validImage = false;
    }
  }
  if (typeof (arrayImagenes) == "object") {
    for (let i = 0; i < arrayImagenes.length; i++) {
      let arr = arrayImagenes[i].split('.');
      if (arr[1] != 'png' && arr[1] != 'jpg' && arr[1] != 'jpeg') {
        validImage = false;
      }
    }
  }
  if (imagen == "") {
    validImage = true;
  }

  const errores = [];
  if (nombre.trim() === "") {
    errores.push({ mensaje: "El nombre no debe estar vacío" });
  }
  if (direccion.trim() === "") {
    errores.push({ mensaje: "La direccion no debe estar vacía" });
  }
  if (telefono.trim() === "") {
    errores.push({ mensaje: "El telefono no debe estar vacío" });
  }
  if (correo.trim() === "") {
    errores.push({ mensaje: "El correo no debe estar vacío" });
  }
  if (id_grt === undefined) {
    errores.push({ mensaje: "Seleccione un gerente" });
  }
  if (habitaciones === undefined) {
    errores.push({ mensaje: "Seleccione minimo una habitación" });
  }
  if (validImage === false) {
    errores.push({ mensaje: "Sólo se permiten los formatos .png .jpg y .jpeg" });
  }
  if (imagen === "" && imagenes === undefined) {
    errores.push({ mensaje: "Selecciona al menos una imagen" });
  }
  if (errores.length > 0) {
    const { id_htl } = req.params;
    let arrayGerentes = [];
    let arrayHoteles = [];

    const hotel = await Hotel.findOne({
      where: {
        id_htl
      }
    });

    let gerentes = await Gerente.findAll();
    arrayGerentes = gerentes.map(e => e.dataValues);
    let hoteles = await Hotel.findAll();
    arrayHoteles = hoteles.map(e => e.dataValues);

    arrayHoteles.map(e => {
      if (e.id_grt != null) {
        let temporal = arrayGerentes.filter(element => element.id_grt != e.id_grt)
        arrayGerentes = temporal;
      }
    })

    if (hotel.dataValues.id_grt != null) {
      const grt = await Gerente.findOne({
        where: {
          id_grt: hotel.dataValues.id_grt
        }
      });

      hotel.dataValues.id_grt = grt.dataValues.nombre;

      arrayGerentes.unshift(grt);
    }

    const hbt = await Habitacion.findAll({
      where: {
        id_htl: id_htl
      }
    });

    let habitaciones = await Habitacion.findAll({
      where: {
        id_htl: null
      }
    });
    if (hbt.length === 1) {
      habitaciones.unshift(hbt[0]);
    } else {
      for (let i = 0; i < hbt.length; i++) {
        habitaciones.unshift(hbt[i]);
      }
    }

    let imagenes = await Hotel_Imagen.findAll({
      where: {
        id_htl
      }
    });

    res.render("editHotel", {
      hotel: hotel,
      gerentes: arrayGerentes,
      habitaciones: habitaciones,
      imagenes, errores
    })
  } else {

    let hotel = await Hotel.findOne({
      where: {
        id_htl
      }
    })

    await Habitacion.update({ id_htl: null }, {
      where: {
        id_htl
      }
    });

    let grt = await Gerente.findAll({
      where: {
        nombre: id_grt
      }
    })

    grt.map(e => console.log(id_grt = e.id_grt))

    await Hotel.update(
      { nombre, direccion, telefono, correo, id_grt },
      {
        where: {
          id_htl
        },
      }
    );

    await Habitacion.update({ id_htl: id_htl }, {
      where: {
        tipo: habitaciones
      }
    });

    let promesa = true;

    let imgs = await Hotel_Imagen.findAll({
      where: {
        id_htl
      }
    })

    if (typeof (imagen) == "string") {
      imgs.map(e => {
        if (imagen == e.dataValues.nombre) {
          console.log('se repiten');
          promesa = false
        }
      })
    }
    if (typeof (imagen) == "object") {
      for (let i = 0; i < imagen.length; i++) {
        imgs.map(e => {
          if (e.dataValues.nombre == imagen[i]) {
            console.log('se repiteeeee');
            promesa = false;
          }
        })
      }
    }

    if (promesa) {
      if (typeof (imagenes) === "undefined") {
        await Hotel_Imagen.update({ id_htl: null }, {
          where: {
            id_htl
          }
        })
      }
      else {

        let getImage = await Hotel_Imagen.findAll({
          where: {
            nombre: imagenes
          }
        })
        getImage = getImage.map(e => e.dataValues.nombre)

        let image = await Hotel_Imagen.update({ id_htl: null }, {
          where: {
            nombre: {
              [Op.not]: imagenes
            },
            id_htl: id_htl
          }
        })

      }
      if (typeof (imagen) == "string") {
        await Hotel_Imagen.create({
          nombre: imagen,
          id_htl: hotel.dataValues.id_htl
        })
      }
      if (typeof (imagen) == "object") {
        for (let i = 0; i < imagen.length; i++) {
          await Hotel_Imagen.create({
            nombre: imagen[i],
            id_htl: hotel.dataValues.id_htl
          })
        }
      }

      await Hotel_Imagen.destroy({
        where: {
          nombre: ''
        }
      });
    }

    res.redirect("/hotel");

  };
}

const removeHotel = async (req, res) => {
  const { id_htl } = req.params;

  let habs = await Hotel_Imagen.update({ id_htl: null }, {
    where: {
      id_htl: id_htl
    }
  });

  let hotel = await Hotel.findOne({
    where: {
      id_htl
    }
  })

  let hbt = await Habitacion.findAll({
    where: {
      id_htl
    }
  })
  hbt.map(async e => {
    await Habitacion_Imagen.destroy({
      where: {
        id_hbt: e.dataValues.id_hbt
      }
    })
  })

  await Habitacion.destroy({
    where: {
      id_htl
    }
  })

  await Hotel.destroy({
    where: {
      id_htl
    }
  });

  let grt = await Gerente.findOne({
    where: {
      id_grt: hotel.dataValues.id_grt
    }
  })

  if (grt != null) {
    await Gerente_Imagen.destroy({
      where: {
        id_grt: grt.dataValues.id_grt
      }
    })
  }

  await Gerente.destroy({
    where: {
      id_grt: hotel.dataValues.id_grt
    }
  })

  res.redirect("/hotel");
};

export { getHoteles, editHoteles, removeHotel, setHoteles };