import express, { Router } from "express";

import { home, login, cerrarSesion } from "../controllers/PagesController.js";
import { editHoteles, getAddHoteles, getEditHoteles, getHoteles, removeHotel, setHoteles } from "../controllers/HotelController.js";
import { editGerente, getAddGerentes, getEditGerente, getGerentes, removeGerente, setGerente } from "../controllers/GerenteController.js";
import { editHabitacion, getAddHabitacion, getEditHabitacion, getHabitaciones, removeHabitacion, setHabitacion } from "../controllers/HabitacionController.js";

const routes = express.Router();

routes.get("/", login)
routes.get("/home", home);
routes.get("/cerrarSesion", cerrarSesion);

routes.post("/hotel", setHoteles);
routes.get("/hotel", getHoteles);
routes.get("/hotel/add", getAddHoteles);
routes.get("/hotel/:id_htl", removeHotel);
routes.get("/hotel/edit/:id_htl", getEditHoteles);
routes.post("/hotel/edit/:id_htl", editHoteles);

routes.post("/gerente", setGerente);
routes.get("/gerente", getGerentes);
routes.get("/gerente/add", getAddGerentes);
routes.get("/gerente/:id_grt", removeGerente);
routes.get("/gerente/edit/:id_grt", getEditGerente);
routes.post("/gerente/edit/:id_grt", editGerente);

routes.get("/habitacion", getHabitaciones);
routes.post("/habitacion", setHabitacion);
routes.get("/habitacion/add", getAddHabitacion);
routes.get("/habitacion/edit/:id_hbt", getEditHabitacion);
routes.post("/habitacion/edit/:id_hbt", editHabitacion);
routes.get("/habitacion/:id_hbt", removeHabitacion);

export default routes; 